<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Shopera
 * @since Shopera 1.0
 */
?>

		</div><!-- #main -->

    <div class="site-footer-wrapper">
        <div class="site-footer-container container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 footer_block footer_block__2_1">
                        <div class="footer_item footer_item__contacts">
                            <h3>Address</h3>
                            <div>
                                <p>4578 Marmora St, San Francisco D04 89GR</p>
                                <p>1-800-1234-567</p>
                                <p>info@demolink.org</p></div>
                        </div>
                    </div>
                    <div class="col-sm-6 footer_block ">
                        <div class="footer_item footer_item__social">
                            <h3>FOLLOW US</h3>
                            <ul>
                                <li><a href="https://www.facebook.com/TemplateMonster/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="//twitter.com/templatemonster"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/tm.opencart/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.youtube.com/user/TemplateMonsterCo"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<!--				<footer id="colophon" class="site-footer row" role="contentinfo">-->
<!--					--><?php
//						if ( is_null(get_sidebar( 'footer' )) && get_option( 'show_on_front' ) != 'page' ) {
//							get_template_part( 'demo-content/footer' );
//						} else {
//							get_sidebar( 'footer' );
//						}
//					?>
<!--				</footer><!-- #colophon -->
<!--				<div class="clearfix"></div>-->
			</div>
<!--			<div class="site-info col-sm-12 col-md-12 col-lg-12">-->
<!--				<div class="site-info-content container">-->
<!--					<div class="copyright">-->
						<?php
//							$copyright = get_theme_mod( 'shopera_copyright' );
//							if ( $copyright ) {
//								echo sanitize_text_field( $copyright );
//							}
						?> 
<!--						--><?php //_e( 'Created by', 'shopera' ); ?><!-- <a href="https://cohhe.com/">--><?php //_e( 'Cohhe', 'shopera' ); ?><!--</a>. -->
<!--						--><?php //_e( 'Proudly powered by', 'shopera' ); ?><!-- <a href="--><?php //echo esc_url( 'http://wordpress.org/' ); ?><!--">--><?php //_e( 'WordPress', 'shopera' ); ?><!--</a>-->
<!--					</div>-->
					<?php 
//						$show_scroll_to_top = get_theme_mod( 'shopera_scrolltotop' );
//
//						if ( $show_scroll_to_top ) { ?>
<!--							<a class="scroll-to-top" href="#">--><?php //_e( 'Up', 'shopera' ); ?><!--</a>-->
						<?php
//						}
					?>
<!--				</div>-->
<!--			</div><!-- .site-info -->
		</div>
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>