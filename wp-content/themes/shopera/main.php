<?php
/*
Template Name: Main
*/

get_header();

global $shopera_site_width;
$post_72 = get_post(72);
$post_89 = get_post(89);
?>

    <div id="main-content" class="main-content row">
        <?php
        get_sidebar();
        ?>
        <div id="primary" class="content-area <?php echo esc_attr( $shopera_site_width ); ?>">
            <div id="content" class="site-content" role="main">

                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();

                    // Include the page content template.
                    get_template_part( 'content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
                endwhile;
                ?>
            </div><!-- #content -->
        </div><!-- #primary -->
        <?php get_sidebar( 'content' ); ?>
    </div><!-- #main-content -->
<?php
get_footer();

